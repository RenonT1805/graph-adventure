package draw

import "github.com/hajimehoshi/ebiten/v2"

type Drawable struct {
	Layer     int
	Invisible bool
	image     *ebiten.Image
	option    *ebiten.DrawImageOptions
}

type IDrawable interface {
	GetLayer() int
	IsInvisible() bool
	GetImage() *ebiten.Image
	GetOption() *ebiten.DrawImageOptions
}

func (d *Drawable) GetLayer() int {
	return d.Layer
}

func (d *Drawable) IsInvisible() bool {
	return d.Invisible
}

func (d *Drawable) GetImage() *ebiten.Image {
	return d.image
}

func (d *Drawable) GetOption() *ebiten.DrawImageOptions {
	return d.option
}

func (d *Drawable) SetImage(image *ebiten.Image, option *ebiten.DrawImageOptions) {
	d.image = image
	d.option = option
}
