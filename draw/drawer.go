package draw

import (
	"sort"

	"github.com/hajimehoshi/ebiten/v2"
)

func WithDrawer[T IDrawable](value T, drawer *Drawer) T {
	drawer.Add(value)
	return value
}

type Drawer struct {
	drawables []IDrawable
}

func (d *Drawer) Add(drawable IDrawable) {
	d.drawables = append(d.drawables, drawable)
}

func (d *Drawer) Draw(screen *ebiten.Image) {
	sort.SliceStable(d.drawables, func(i, j int) bool {
		return d.drawables[i].GetLayer() < d.drawables[j].GetLayer()
	})
	for _, drawable := range d.drawables {
		if drawable.IsInvisible() || drawable.GetImage() == nil {
			continue
		}
		screen.DrawImage(drawable.GetImage(), drawable.GetOption())
	}
}
