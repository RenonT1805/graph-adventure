package main

import (
	_ "graph-adventure/assets"
	"graph-adventure/config"
	"graph-adventure/constants"
	"graph-adventure/imguiw"
	"graph-adventure/scenes"
	"log"

	"github.com/hajimehoshi/ebiten/v2"
)

var sm scenes.SceneManager

type Game struct {
}

func (g *Game) Update() error {
	return sm.Update(constants.FRESH_RATE)
}

func (g *Game) Draw(screen *ebiten.Image) {
	sm.Draw(screen)
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	imguiw.Manager.SetDisplaySize(float32(constants.LAYOUT_WIDTH), float32(constants.LAYOUT_HEIGHT))
	return constants.LAYOUT_WIDTH, constants.LAYOUT_HEIGHT
}

func main() {
	imguiw.Init()

	sm.Regist("Title", &scenes.Title{})
	sm.Change("Title", false)

	config := config.GetDefaultConfig()
	ebiten.SetWindowSize(
		constants.SCREEN_WIDTH,
		constants.SCREEN_HEIGHT,
	)
	ebiten.SetWindowTitle(config.AppName)
	if err := ebiten.RunGame(&Game{}); err != nil {
		log.Fatal(err)
	}
}
