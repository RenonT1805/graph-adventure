%{
package formula

import "strconv"

%}

%union{
    token Token
    expression Expression
}

%type<expression> formula
%type<expression> expression

%token<token> IDENT
%token<token> VALUE
%token<token> SYNTAX_DESCRIPTOR
%token<token> ARITHMETIC_SYMBOLS
%token<token> ARITHMETIC_KEYWORDS

%left '+' '-'
%left '*' '/' '%' '^'
%%

formula
    : expression
    {
        if l, isLexer := yylex.(*Lexer); isLexer {
			l.Formula = $1
		}
    }

expression
    : VALUE
    {
        f, err := strconv.ParseFloat($1.literal, 64)
        if err != nil {
            panic(err)
        }
        $$ = Expression{
            Type: Value,
            Terms: []Term{f},
        }
    }
    | IDENT
    {
         $$ = Expression{
            Type: Variable,
            Terms: []Term{
                Var{
                    Name: $1.literal,
                },
            },
        }
    }
    | '(' expression ')'
    {
        $$ = $2
    }
    | '{' expression '}'
    {
        $$ = $2
    }
    | expression '+' expression
    {
        $$ = Expression{Type: Addition, Terms: []Term{$1, $3}}
    }
    | expression '-' expression
    {
        $$ = Expression{Type: Subtraction, Terms: []Term{$1, $3}}
    }
    | expression '*' expression
    {
        $$ = Expression{Type: Multiplication, Terms: []Term{$1, $3}}
    }
    | expression '/' expression
    {
        $$ = Expression{Type: Division, Terms: []Term{$1, $3}}
    }
    | expression '%' expression
    {
        $$ = Expression{Type: Modulo, Terms: []Term{$1, $3}}
    }
    | expression '^' expression
    {
        $$ = Expression{Type: Power, Terms: []Term{$1, $3}}
    }
    | ARITHMETIC_KEYWORDS '(' expression ')'
    {
        $$ = Expression{
            Type: stringToExpressionType($1.literal),
            Terms: []Term{$3},
        }
    }
    | '-' expression
    {
        $$ = Expression{
            Type: Multiplication, 
            Terms: []Term{
                Expression{
                    Type: Value,
                    Terms: []Term{-1.0},
                },
                $2,
            },
        }
    }

%%