package formula

type ExpressionType int

type Term = interface{}
type Variables = map[string]float64

type Expression struct {
	Type  ExpressionType
	Terms []Term
}

type Var struct {
	Name string
}

const (
	InvalidExpressionType ExpressionType = iota + 1

	Value    // value
	Variable // value

	Addition       // +
	Subtraction    // -
	Multiplication // *
	Division       // /
	Modulo         // %
	Power          // ^
	LogN           // logN

	Sqrt  // sqrt
	Log   // log
	LN    // ln
	Exp   // exp
	Abs   // abs
	Sin   // sin
	Cos   // cos
	Tan   // tan
	ASin  // asin
	ACos  // acos
	ATan  // atan
	SinH  // sinh
	CosH  // cosh
	TanH  // tanh
	ASinH // asinh
	ACosH // acosh
	ATanH // atanh
	Floor // floor
)

func stringToExpressionType(src string) ExpressionType {
	switch src {
	case "+":
		return Addition
	case "-":
		return Subtraction
	case "*":
		return Multiplication
	case "/":
		return Division
	case "%":
		return Modulo
	case "^":
		return Power
	case "logN":
		return LogN
	case "sqrt":
		return Sqrt
	case "log":
		return Log
	case "ln":
		return LN
	case "exp":
		return Exp
	case "abs":
		return Abs
	case "sin":
		return Sin
	case "cos":
		return Cos
	case "tan":
		return Tan
	case "asin":
		return ASin
	case "acos":
		return ACos
	case "atan":
		return ATan
	case "sinh":
		return SinH
	case "cosh":
		return CosH
	case "tanh":
		return TanH
	case "asinh":
		return ASinH
	case "acosh":
		return ACosH
	case "atanh":
		return ATanH
	case "floor":
		return Floor
	}
	return InvalidExpressionType
}
