package formula

import (
	"errors"
	"math"

	"gitlab.com/RenonT1805/rexer"
)

type Token struct {
	token   int
	literal string
}

type Lexer struct {
	rexer.Lexer
	Formula Expression
	err     error
}

func (l *Lexer) Lex(lval *yySymType) int {
	token := l.NextToken()
	if token == nil {
		return 0
	}

	if (token.TokenType == SYNTAX_DESCRIPTOR || token.TokenType == ARITHMETIC_SYMBOLS) && len(token.Literal) == 1 {
		token.TokenType = int(token.Literal[0])
	}

	lval.token = Token{token: token.TokenType, literal: token.Literal}
	return token.TokenType
}

func (l *Lexer) Error(e string) {
	l.err = errors.New(e)
}

func Calculation(exp Expression, variables Variables) float64 {
	switch exp.Type {
	case Value:
		return exp.Terms[0].(float64)
	case Variable:
		return variables[exp.Terms[0].(Var).Name]
	case Addition:
		return Calculation(exp.Terms[0].(Expression), variables) + Calculation(exp.Terms[1].(Expression), variables)
	case Subtraction:
		return Calculation(exp.Terms[0].(Expression), variables) - Calculation(exp.Terms[1].(Expression), variables)
	case Multiplication:
		return Calculation(exp.Terms[0].(Expression), variables) * Calculation(exp.Terms[1].(Expression), variables)
	case Division:
		return Calculation(exp.Terms[0].(Expression), variables) / Calculation(exp.Terms[1].(Expression), variables)
	case Modulo:
		return math.Mod(Calculation(exp.Terms[0].(Expression), variables), Calculation(exp.Terms[1].(Expression), variables))
	case Power:
		return math.Pow(Calculation(exp.Terms[0].(Expression), variables), Calculation(exp.Terms[1].(Expression), variables))
	case LogN:
		panic("no support formula")
	case Sqrt:
		return math.Sqrt(Calculation(exp.Terms[0].(Expression), variables))
	case Log:
		return math.Log10(Calculation(exp.Terms[0].(Expression), variables))
	case LN:
		return math.Log(Calculation(exp.Terms[0].(Expression), variables))
	case Exp:
		return math.Exp(Calculation(exp.Terms[0].(Expression), variables))
	case Abs:
		return math.Abs(Calculation(exp.Terms[0].(Expression), variables))
	case Sin:
		return math.Sin(Calculation(exp.Terms[0].(Expression), variables))
	case Cos:
		return math.Cos(Calculation(exp.Terms[0].(Expression), variables))
	case Tan:
		return math.Tan(Calculation(exp.Terms[0].(Expression), variables))
	case ASin:
		return math.Asin(Calculation(exp.Terms[0].(Expression), variables))
	case ACos:
		return math.Acos(Calculation(exp.Terms[0].(Expression), variables))
	case ATan:
		return math.Atan(Calculation(exp.Terms[0].(Expression), variables))
	case SinH:
		return math.Sinh(Calculation(exp.Terms[0].(Expression), variables))
	case CosH:
		return math.Cosh(Calculation(exp.Terms[0].(Expression), variables))
	case TanH:
		return math.Tanh(Calculation(exp.Terms[0].(Expression), variables))
	case ASinH:
		return math.Asinh(Calculation(exp.Terms[0].(Expression), variables))
	case ACosH:
		return math.Acosh(Calculation(exp.Terms[0].(Expression), variables))
	case ATanH:
		return math.Atanh(Calculation(exp.Terms[0].(Expression), variables))
	case Floor:
		return math.Floor(Calculation(exp.Terms[0].(Expression), variables))
	}
	return 0
}

func ParseFormulaText(formula string) (*Expression, error) {
	l := Lexer{}
	if tmp := rexer.NewLexer(formula); tmp != nil {
		tmp.SkipTokenType = -1
		tmp.IdentTokenType = IDENT
		tmp.Symbols = []rexer.Symbol{
			rexer.NewPatternSymbol(-1, []string{" ", "\t", "\r", "\n"}),
			rexer.NewRegexSymbol(VALUE, []string{"[0-9]+(?:\\.[0-9]+)?"}),
			rexer.NewPatternSymbol(SYNTAX_DESCRIPTOR, []string{"(", ")", "{", "}"}),
			rexer.NewPatternSymbol(ARITHMETIC_SYMBOLS, []string{
				"+",
				"-",
				"*",
				"/",
				"%",
				"^",
			}),
			rexer.NewPatternSymbol(ARITHMETIC_KEYWORDS, []string{
				"sqrt",
				"log",
				"ln",
				"exp",
				"abs",
				"sinh",
				"cosh",
				"tanh",
				"sin",
				"cos",
				"tan",
				"asin",
				"acos",
				"atan",
				"floor",
			}),
		}
		l.Lexer = *tmp
		yyParse(&l)
	}

	if l.err != nil {
		return nil, l.err
	}

	return &l.Formula, nil
}
