package resources

import (
	"errors"
	"graph-adventure/utils"
	"log"
)

var resourceStorage = map[string]map[string]interface{}{}

func GetResource[T any](key string) *T {
	v, err := GetResourceWithErr[T](key)
	if err != nil {
		log.Panicln(err)
	}
	return v
}

func GetResourceWithErr[T any](key string) (*T, error) {
	t := utils.GetTypeName[T]()
	resources, exist := resourceStorage[t]
	if !exist {
		return nil, errors.New("resource '" + t + "' is not register")
	}
	var resource interface{}
	resource, exist = resources[key]
	if !exist {
		return nil, errors.New("resource '" + key + "' is not found")
	}
	result := resource.(*T)
	return result, nil
}

func SetResource[T any](key string, value *T) {
	t := utils.GetTypeName[T]()
	resources, exist := resourceStorage[t]
	if !exist {
		resourceStorage[t] = map[string]interface{}{}
		resources = resourceStorage[t]
	}
	resources[key] = value
}
