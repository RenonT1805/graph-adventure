package config

type Config struct {
	AppName    string
	LowAppName string
}

func GetDefaultConfig() Config {
	return Config{
		AppName:    "GraphAdventure",
		LowAppName: "graph-adventure",
	}
}
