package imguiw

import (
	"github.com/gabstv/ebiten-imgui/renderer"
)

var (
	Manager *renderer.Manager
)

func Init() {
	Manager = renderer.New(nil)
}
