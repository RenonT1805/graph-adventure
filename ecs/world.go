package ecs

type World struct {
	Systems       [][]System
	Chunks        map[string]interface{}
	EntityRemover map[Entity]map[string]func(*World, Entity) error
}

func CreateWorld() *World {
	return &World{
		Systems:       [][]System{},
		Chunks:        map[string]interface{}{},
		EntityRemover: map[Entity]map[string]func(*World, Entity) error{},
	}
}

func Update(w *World, dt float64) error {
	for i := range w.Systems {
		errs := make(chan error, len(w.Systems[i]))
		for j := range w.Systems[i] {
			go func(system *System) {
				errs <- (*system).Update(w, dt)
			}(&w.Systems[i][j])
		}
		for j := 0; j < len(w.Systems[i]); j++ {
			err := <-errs
			if err != nil {
				return err
			}
		}
	}
	return nil
}
