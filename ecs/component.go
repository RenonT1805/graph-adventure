package ecs

import (
	"errors"
	"graph-adventure/utils"
)

type Chunk[T any] struct {
	Components map[Entity]*T
}

func getChunk[T any](world *World) (*Chunk[T], error) {
	if chunk, exist := world.Chunks[utils.GetTypeName[T]()]; exist {
		return chunk.(*Chunk[T]), nil
	}
	return nil, errors.New("chunk '" + utils.GetTypeName[T]() + "' is not found")
}

func GetComponents[T any](world *World) map[Entity]*T {
	chunk, err := getChunk[T](world)
	if err != nil {
		return map[Entity]*T{}
	}
	return chunk.Components
}

func GetComponent[T any](world *World, entity Entity) (*T, error) {
	chunk, err := getChunk[T](world)
	if err != nil {
		return nil, err
	}
	if value, exist := chunk.Components[entity]; exist {
		return value, nil
	}
	return nil, errors.New("component '" + utils.GetTypeName[T]() + "' is not found in entity '" + entity.ToString() + "'")
}

func RemoveComponent[T any](world *World, entity Entity) error {
	chunk, err := getChunk[T](world)
	if err != nil {
		return err
	}
	if _, exist := chunk.Components[entity]; exist {
		delete(chunk.Components, entity)
		return nil
	}
	return errors.New("component '" + utils.GetTypeName[T]() + "' is not found in entity '" + entity.ToString() + "'")
}

func AddComponent[T any](world *World, entity Entity, value *T) {
	componentName := utils.GetTypeName[T]()
	chunk, err := getChunk[T](world)
	if err != nil {
		chunk = &Chunk[T]{
			Components: map[Entity]*T{},
		}
		world.Chunks[componentName] = chunk
	}
	chunk.Components[entity] = value

	remover, exist := world.EntityRemover[entity]
	if !exist {
		remover = map[string]func(*World, Entity) error{}
	}
	remover[componentName] = RemoveComponent[T]
	world.EntityRemover[entity] = remover
}
