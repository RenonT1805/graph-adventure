package ecs

import (
	"graph-adventure/utils"
	"reflect"
	"sort"
)

type System interface {
	Priority() int64
	Update(world *World, dt float64) error
}

type SystemInitializer = func(w *World) System

func AddSystem(w *World, initializer SystemInitializer) {
	system := initializer(w)
	jud := false
	for i := range w.Systems {
		if w.Systems[i][0].Priority() == system.Priority() {
			w.Systems[i] = append(w.Systems[i], system)
			jud = true
			break
		}
	}
	if !jud {
		sys := []System{system}
		w.Systems = append(w.Systems, sys)
		sort.SliceStable(w.Systems, func(i, j int) bool { return w.Systems[i][0].Priority() < w.Systems[j][0].Priority() })
	}
}

func RemoveSystem[T any](w *World) {
	var system T
	for j, sys := range w.Systems {
		for i, s := range sys {
			if reflect.TypeOf(s).String() != reflect.TypeOf(system).String() {
				continue
			}
			w.Systems[j] = utils.RemoveSliceItem(w.Systems[j], i)
		}
	}
}
