package ecs

import (
	"errors"
	"strconv"
)

type Entity int64

func (e Entity) ToString() string {
	return strconv.FormatInt(int64(e), 10)
}

func RemoveEntity(w *World, entity Entity) error {
	remover, exist := w.EntityRemover[entity]
	if !exist {
		return errors.New("entity '" + entity.ToString() + "' is not found")
	}
	for _, rm := range remover {
		rm(w, entity)
	}
	return nil
}
