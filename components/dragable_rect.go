package components

import (
	"graph-adventure/draw"
)

type DragableRect struct {
	draw.Drawable
}
