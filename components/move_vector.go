package components

import "graph-adventure/ecs"

type MoveVector struct {
	X, Y   float64
	Entity ecs.Entity
}
