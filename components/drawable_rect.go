package components

import (
	"graph-adventure/draw"
	"image/color"
)

type DrawableRect struct {
	draw.Drawable
	BorderWidth     float64
	BorderColor     color.Color
	BackgroundColor color.Color
}
