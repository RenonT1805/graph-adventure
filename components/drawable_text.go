package components

import (
	"graph-adventure/draw"
	"image/color"

	"golang.org/x/image/font"
)

type CursorStyle struct {
	CursorColor      *color.Color
	RangeSelectColor color.Color
	CursorWidth      float64
	CursorHeight     float64
	CursorXOffset    float64
	CursorYOffset    float64
}

type DrawableTextOnRect struct {
	draw.Drawable
	Font        *font.Face
	TextColor   color.Color
	Row         int
	XOffset     float64
	YOffset     float64
	ViewXOffset float64
	ViewYOffset float64
	CursorStyle *CursorStyle
}

type DrawableTextAttachToRect struct {
	draw.Drawable
	Font           *font.Face
	TextColor      color.Color
	ScaleX, ScaleY float64
}
