package components

import (
	"graph-adventure/draw"

	_ "image/png"

	"github.com/hajimehoshi/ebiten/v2"
)

type DrawableImageAttachToRect struct {
	draw.Drawable
	Image *ebiten.Image
}
