package components

type Rect struct {
	Position
	Height, Width float64
}

func (r Rect) OnRect(X, Y float64) bool {
	return r.X <= X && X <= r.X+r.Width && r.Y <= Y && Y <= r.Y+r.Height
}

func (r Rect) PositionIsOnRect(pos Position) bool {
	return r.X <= pos.X && pos.X <= r.X+r.Width && r.Y <= pos.Y && pos.Y <= r.Y+r.Height
}
