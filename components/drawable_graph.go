package components

import (
	"graph-adventure/draw"
	"graph-adventure/formula"
	"image/color"
)

type Graph struct {
	Expression         formula.Expression
	GraphColor         color.Color
	TransitionVariable string
	OriginX, OriginY   float64
}

type DrawableGraphAttachToRect struct {
	draw.Drawable
	Resolution   int
	SizeX, SizeY float64
	Direction    float64
	Variables    formula.Variables
	Graphs       []Graph
}
