package components

import (
	"graph-adventure/ecs"
)

type ClickableRect struct {
	Action func(*ecs.World, ecs.Entity)
}
