package components

type Inputable struct {
	Always                bool
	Active                bool
	ShowCursor            bool
	CursorPos             int
	PressingTime          int
	PressingInputInterval int
	AllowNewLine          bool

	Mark int
}

func (i *Inputable) IsRangeSelected() bool {
	return i.Mark != -1 && i.Mark != i.CursorPos
}

func (i *Inputable) ResetMark() {
	i.Mark = -1
}

func (i *Inputable) SetMark(pos int) {
	i.Mark = pos
}

func (i *Inputable) GetSelectRange() (int, int) {
	if !i.IsRangeSelected() {
		return -1, -1
	}
	if i.Mark < i.CursorPos {
		return i.Mark, i.CursorPos
	}
	return i.CursorPos, i.Mark
}
