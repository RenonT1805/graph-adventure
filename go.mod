module graph-adventure

go 1.18

require (
	github.com/gabstv/ebiten-imgui v0.5.0
	github.com/hajimehoshi/ebiten/v2 v2.3.7
	github.com/inkyblackness/imgui-go/v4 v4.5.0
	gitlab.com/RenonT1805/rexer v0.0.0-20210727234725-b0d612af0894
	golang.org/x/image v0.0.0-20220321031419-a8550c1d254a
)

require (
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20220320163800-277f93cfa958 // indirect
	github.com/gofrs/flock v0.8.1 // indirect
	github.com/jezek/xgb v1.0.0 // indirect
	golang.org/x/exp v0.0.0-20190731235908-ec7cb31e5a56 // indirect
	golang.org/x/mobile v0.0.0-20220518205345-8578da9835fd // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20220408201424-a24fb2fb8a0f // indirect
	golang.org/x/text v0.3.7 // indirect
)
