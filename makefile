EXENAME=graph-adventure.exe
PARSER_SOURCE_PATH=formula\formula_parser.go.y
PARSER_OUTPUT_PATH=formula\formula_parser.go

build-parser:
	goyacc -o $(PARSER_OUTPUT_PATH) $(PARSER_SOURCE_PATH)

run:
	make build-parser
	go build -o $(EXENAME)
	./$(EXENAME)
