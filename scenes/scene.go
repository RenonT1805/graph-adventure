package scenes

import (
	"errors"

	"github.com/hajimehoshi/ebiten/v2"
)

type Scene interface {
	Init() error
	Update(dt float64) error
	Draw(screen *ebiten.Image)
}

type SceneManager struct {
	scenes map[string]Scene
	stack  []string
}

func (sm *SceneManager) Update(dt float64) error {
	sceneNum := len(sm.stack)
	if sceneNum == 0 {
		return errors.New("active scene not exist")
	}
	return sm.scenes[sm.stack[sceneNum-1]].Update(dt)
}

func (sm *SceneManager) Draw(screen *ebiten.Image) {
	for _, scene := range sm.stack {
		sm.scenes[scene].Draw(screen)
	}
}

func (sm *SceneManager) Push(name string, initialize bool) error {
	sm.stack = append(sm.stack, name)
	if initialize {
		return sm.scenes[sm.stack[len(sm.stack)-1]].Init()
	}
	return nil
}

func (sm *SceneManager) Pop() error {
	if len(sm.stack) <= 1 {
		return errors.New("stack scene not exist")
	}
	sm.stack = sm.stack[:len(sm.stack)-1]
	return nil
}

func (sm *SceneManager) Change(name string, initialize bool) error {
	if len(sm.stack) > 1 {
		return errors.New("stack scene exist")
	}
	sm.stack = []string{name}
	if initialize {
		return sm.scenes[name].Init()
	}
	return nil
}

func (sm *SceneManager) Regist(name string, scene Scene) {
	if sm.scenes == nil {
		sm.scenes = map[string]Scene{}
	}
	sm.scenes[name] = scene
	sm.scenes[name].Init()
}
