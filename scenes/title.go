package scenes

import (
	"fmt"
	"graph-adventure/components"
	"graph-adventure/constants"
	"graph-adventure/draw"
	"graph-adventure/ecs"
	"graph-adventure/formula"
	"graph-adventure/resources"
	"graph-adventure/systems"
	"image/color"
	"math"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"golang.org/x/image/font"
)

type Title struct {
	world  *ecs.World
	drawer *draw.Drawer

	backgroundEntity       ecs.Entity
	menuEntity             ecs.Entity
	xFormulaInputBoxEntity ecs.Entity
	yFormulaInputBoxEntity ecs.Entity
	graphEntity            ecs.Entity
	markerEntities         []ecs.Entity
	yoyoEntity             ecs.Entity
	playerEntity           ecs.Entity

	markerFlip bool
	markerX    float64
	variables  formula.Variables
}

func (s *Title) Init() error {
	s.drawer = &draw.Drawer{}
	s.world = ecs.CreateWorld()
	s.variables = formula.Variables{}

	///// System登録

	ecs.AddSystem(s.world, systems.GetDefaultTextInputInitializer())
	ecs.AddSystem(s.world, systems.GetDefaultDrawGraphInitializer(s.drawer))
	ecs.AddSystem(s.world, systems.GetDefaultDrawTextOnRectInitializer(s.drawer))
	ecs.AddSystem(s.world, systems.GetDefaultDrawImageInitializer(s.drawer))
	ecs.AddSystem(s.world, systems.GetDefaultDrawRectInitializer(s.drawer))
	ecs.AddSystem(s.world, systems.GetDefaultDrawTextAttachToRectInitializer(s.drawer))
	ecs.AddSystem(s.world, systems.GetDefaultLinerMoveInitializer())
	ecs.AddSystem(s.world, systems.GetDefaultRectClickInitializer())
	ecs.AddSystem(s.world, systems.GetDefaultRectDragInitializer())

	///// Entity登録

	s.backgroundEntity = 0
	s.menuEntity = 1
	s.xFormulaInputBoxEntity = 2
	s.yFormulaInputBoxEntity = 3
	s.graphEntity = 4
	s.markerEntities = []ecs.Entity{-10, -20}
	s.yoyoEntity = 5
	s.playerEntity = 6

	///// Components登録

	// 背景表示
	ecs.AddComponent(s.world, s.backgroundEntity, &components.Rect{
		Width:  constants.LAYOUT_WIDTH,
		Height: constants.LAYOUT_HEIGHT,
	})
	ecs.AddComponent(s.world, s.backgroundEntity, draw.WithDrawer(&components.DrawableRect{
		BackgroundColor: color.RGBA{R: 128, G: 128, B: 128, A: 255},
		Drawable: draw.Drawable{
			Layer: -1000,
		},
	}, s.drawer))

	// メニュー背景表示
	ecs.AddComponent(s.world, s.menuEntity, &components.Rect{
		Position: components.Position{
			Y: constants.LAYOUT_HEIGHT - 150.0,
		},
		Width:  constants.LAYOUT_WIDTH,
		Height: 150.0,
	})
	ecs.AddComponent(s.world, s.menuEntity, draw.WithDrawer(&components.DrawableImageAttachToRect{
		Image: resources.GetResource[ebiten.Image]("control_frame"),
		Drawable: draw.Drawable{
			Layer: -10,
		},
	}, s.drawer))

	// InputBox
	addInputBoxComponent := func(entity ecs.Entity, rect *components.Rect, defaultFormula string) {
		ecs.AddComponent(s.world, entity, rect)
		ecs.AddComponent(s.world, entity, &components.Text{
			Text: defaultFormula,
		})
		ecs.AddComponent(s.world, entity, &components.Inputable{
			ShowCursor:            true,
			CursorPos:             len(defaultFormula),
			Mark:                  len(defaultFormula),
			PressingTime:          500,
			PressingInputInterval: 3,
			AllowNewLine:          false,
		})
		ecs.AddComponent(s.world, entity, draw.WithDrawer(&components.DrawableRect{
			BackgroundColor: color.White,
			BorderColor:     color.Black,
			BorderWidth:     1.0,
		}, s.drawer))
		ecs.AddComponent(s.world, entity, draw.WithDrawer(&components.DrawableTextOnRect{
			Font:      resources.GetResource[font.Face]("default_36_72"),
			TextColor: color.Black,
			Row:       1,
			XOffset:   4.0,
			CursorStyle: &components.CursorStyle{
				RangeSelectColor: color.RGBA{R: 64, G: 64, B: 255, A: 128},
				CursorXOffset:    2.0,
				CursorYOffset:    4.0,
				CursorWidth:      2.0,
				CursorHeight:     40.0,
			},
			Drawable: draw.Drawable{
				Layer: 10,
			},
		}, s.drawer))
	}
	addInputBoxComponent(s.xFormulaInputBoxEntity, &components.Rect{
		Position: components.Position{
			X: 25.0,
			Y: 590.0,
		},
		Width:  constants.LAYOUT_WIDTH - 50.0,
		Height: 50.0,
	}, "sin(x)")
	addInputBoxComponent(s.yFormulaInputBoxEntity, &components.Rect{
		Position: components.Position{
			X: 25.0,
			Y: 650.0,
		},
		Width:  constants.LAYOUT_WIDTH - 50.0,
		Height: 50.0,
	}, "cos(x)")

	// Graph
	var (
		graphWidth  = 150.0
		graphHeight = 150.0
	)
	ecs.AddComponent(s.world, s.graphEntity, &components.Rect{
		Position: components.Position{
			X: 10.0,
			Y: 10.0,
		},
		Width:  graphWidth,
		Height: graphHeight,
	})
	ecs.AddComponent(s.world, s.graphEntity, draw.WithDrawer(&components.DrawableRect{
		BackgroundColor: color.White,
		BorderColor:     color.Black,
		BorderWidth:     1,
	}, s.drawer))
	ecs.AddComponent(s.world, s.graphEntity, draw.WithDrawer(&components.DrawableGraphAttachToRect{
		Variables:  map[string]float64{},
		Resolution: int(graphWidth / 2.0),
		SizeX:      10.0,
		SizeY:      10.0,
		Graphs: []components.Graph{
			{
				GraphColor:         color.RGBA{R: 255, G: 0, B: 0, A: 255},
				TransitionVariable: "x",
			},
			{
				GraphColor:         color.RGBA{R: 0, G: 0, B: 255, A: 255},
				TransitionVariable: "x",
			},
		},
		Drawable: draw.Drawable{
			Layer: 10,
		},
	}, s.drawer))

	// Marker
	for _, entity := range s.markerEntities {
		ecs.AddComponent(s.world, entity, &components.Rect{
			Width:  15.0,
			Height: 15.0,
		})
		ecs.AddComponent(s.world, entity, draw.WithDrawer(&components.DrawableImageAttachToRect{
			Image: resources.GetResource[ebiten.Image]("marker"),
			Drawable: draw.Drawable{
				Layer: 20,
			},
		}, s.drawer))
	}

	// YoYo
	ecs.AddComponent(s.world, s.yoyoEntity, &components.Rect{
		Width:  60.0,
		Height: 60.0,
	})
	ecs.AddComponent(s.world, s.yoyoEntity, draw.WithDrawer(&components.DrawableImageAttachToRect{
		Image: resources.GetResource[ebiten.Image]("marker"),
		Drawable: draw.Drawable{
			Layer: 20,
		},
	}, s.drawer))

	// Player
	ecs.AddComponent(s.world, s.playerEntity, &components.Rect{
		Position: components.Position{
			X: 400, Y: 400,
		},
		Width:  60.0,
		Height: 60.0,
	})
	ecs.AddComponent(s.world, s.playerEntity, draw.WithDrawer(&components.DrawableRect{
		BackgroundColor: color.Black,
	}, s.drawer))
	return nil
}

func (s *Title) Update(dt float64) error {
	// 数式解析
	getExpression := func(entity ecs.Entity) (*formula.Expression, error) {
		txt, err := ecs.GetComponent[components.Text](s.world, entity)
		if err != nil {
			return nil, err
		}
		expr, err := formula.ParseFormulaText(txt.Text)
		if err != nil {
			expr = &formula.Expression{
				Type:  formula.Value,
				Terms: []formula.Term{0.0},
			}
		}
		return expr, nil
	}

	// グラフ更新
	graph, err := ecs.GetComponent[components.DrawableGraphAttachToRect](s.world, s.graphEntity)
	if err != nil {
		return err
	}
	expr, err := getExpression(s.xFormulaInputBoxEntity)
	if err != nil {
		return err
	}
	graph.Graphs[0].Expression = *expr
	expr, err = getExpression(s.yFormulaInputBoxEntity)
	if err != nil {
		return err
	}
	graph.Graphs[1].Expression = *expr

	// プレイヤー更新
	playerRect, err := ecs.GetComponent[components.Rect](s.world, s.playerEntity)
	if err != nil {
		return err
	}

	// マーカー更新
	markerSpeed := 10.0
	if s.markerFlip {
		s.markerX -= dt * markerSpeed
	} else {
		s.markerX += dt * markerSpeed
	}
	if s.markerX < 0 || graph.SizeX < s.markerX {
		s.markerFlip = !s.markerFlip
	} else {
		s.markerX = math.Mod(s.markerX, graph.SizeX)
	}
	graphRect, err := ecs.GetComponent[components.Rect](s.world, s.graphEntity)
	if err != nil {
		return err
	}
	calcResults := make([]float64, len(s.markerEntities))
	for i, entity := range s.markerEntities {
		rect, err := ecs.GetComponent[components.Rect](s.world, entity)
		if err != nil {
			return err
		}
		s.variables["x"] = s.markerX - graph.SizeX/2 + graph.Graphs[i].OriginX
		calcResults[i] = formula.Calculation(graph.Graphs[i].Expression, s.variables)
		rect.X = s.markerX*(graphRect.Width/graph.SizeX) - rect.Width/2 + graphRect.X
		rect.Y = (calcResults[i]-graph.SizeY/2+graph.Graphs[i].OriginY)*-(graphRect.Height/graph.SizeY) - rect.Height/2 + graphRect.Y
	}

	// ヨーヨー更新
	yoyoSpeed := 100.0
	yoyoRect, err := ecs.GetComponent[components.Rect](s.world, s.yoyoEntity)
	if err != nil {
		return err
	}
	yoyoRect.X = calcResults[0]*yoyoSpeed + playerRect.X
	yoyoRect.Y = calcResults[1]*yoyoSpeed + playerRect.Y
	fmt.Println(calcResults)

	err = ecs.Update(s.world, dt)
	if err != nil {
		return err
	}
	return nil
}

func (s *Title) Draw(screen *ebiten.Image) {
	s.drawer.Draw(screen)
	ebitenutil.DebugPrint(screen, fmt.Sprintf("TPS: %.2f", ebiten.CurrentTPS()))
}
