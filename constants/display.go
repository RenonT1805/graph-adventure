package constants

const LAYOUT_WIDTH = 1280
const LAYOUT_HEIGHT = 720

const SCREEN_WIDTH = 1280
const SCREEN_HEIGHT = 720

const FRESH_RATE = 1.0 / 60.0
