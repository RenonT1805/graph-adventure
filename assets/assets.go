package assets

import (
	"bytes"
	_ "embed"
	"graph-adventure/resources"
	_ "image/png"
	"log"

	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/examples/resources/fonts"
	"golang.org/x/image/font/opentype"
	"golang.org/x/image/font/sfnt"
)

var (
	//go:embed "img/05 ボタン/button3.png"
	activeButtonFrame []byte
	//go:embed "img/01 メッセージウィンドウ/textframe_wt.png"
	controlFrame []byte
	//go:embed "img/09 パワーゲージ/handle_a.png"
	marker []byte
)

func errorToFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func registImageResource(key string, data []byte) {
	img, _, err := ebitenutil.NewImageFromReader(bytes.NewReader(data))
	errorToFatal(err)
	resources.SetResource(key, img)
}

func registFontResource(key string, tt *sfnt.Font, opt *opentype.FaceOptions) {
	rf, err := opentype.NewFace(tt, opt)
	errorToFatal(err)
	resources.SetResource(key, &rf)
}

func init() {
	// Fonts
	tt, err := opentype.Parse(fonts.MPlus1pRegular_ttf)
	errorToFatal(err)

	registFontResource("default_72_72", tt, &opentype.FaceOptions{Size: 72, DPI: 72})
	registFontResource("default_36_72", tt, &opentype.FaceOptions{Size: 36, DPI: 72})

	// Image
	registImageResource("avtive_button", activeButtonFrame)
	registImageResource("control_frame", controlFrame)
	registImageResource("marker", marker)
}
