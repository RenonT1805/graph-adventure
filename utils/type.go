package utils

import "reflect"

func GetTypeName[T any]() string {
	return reflect.TypeOf((*T)(nil)).String()[1:]
}
