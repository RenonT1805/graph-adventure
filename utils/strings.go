package utils

type LineWrappedText []string

func PosToRowColumn(str string, pos int) (int, int) {
	var row, column int
	for i, c := range str {
		if i == pos {
			break
		}
		column++
		if c == '\n' {
			row++
			column = 0
		}
	}
	return row, column
}

func RowColumnToPos(str string, row, column int) int {
	var p, line int
	for i, c := range str {
		if line == row {
			p = i
			break
		}
		if c == '\n' {
			p = i + 1
			line++
		}
	}
	return p + column
}

func InsertString(s, v string, i int) string {
	return s[:i] + v + s[i:]
}

func RemoveString(s string, start, end int) string {
	return s[:start] + s[end:]
}
