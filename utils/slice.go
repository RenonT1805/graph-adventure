package utils

func RemoveSliceItem[T any](s []T, i int) []T {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}

func RemoveOrderdSliceItem[T any](s []T, i int) []T {
	return append(s[:i], s[i+1:]...)
}

func InsertSliceItem[T any](s, v []T, i int) []T {
	ary := make([]T, len(s)+len(v))
	copy(ary, s[:i])
	copy(ary[i:], v)
	copy(ary[i+len(v):], s[i:])
	return ary
}
