package systems

import (
	"graph-adventure/components"
	"graph-adventure/ecs"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

func GetDefaultRectClickInitializer() ecs.SystemInitializer {
	return func(w *ecs.World) ecs.System {
		return &RectClick{}
	}
}

type RectClick struct {
	justClick        bool
	cursorX, cursorY int
}

func (s *RectClick) update(world *ecs.World, entity ecs.Entity, dt float64, component *components.ClickableRect) error {
	rect, err := ecs.GetComponent[components.Rect](world, entity)
	if err != nil {
		return err
	}
	if !s.justClick || !rect.OnRect(float64(s.cursorX), float64(s.cursorY)) {
		return nil
	}
	component.Action(world, entity)
	return nil
}

func (s RectClick) Priority() int64 {
	return 0
}

func (s *RectClick) Update(world *ecs.World, dt float64) error {
	s.justClick = inpututil.IsMouseButtonJustPressed(ebiten.MouseButtonLeft)
	s.cursorX, s.cursorY = ebiten.CursorPosition()

	components := ecs.GetComponents[components.ClickableRect](world)
	for entity, component := range components {
		err := s.update(world, entity, dt, component)
		if err != nil {
			return err
		}
	}
	return nil
}
