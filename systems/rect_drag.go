package systems

import (
	"graph-adventure/components"
	"graph-adventure/ecs"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

func GetDefaultRectDragInitializer() ecs.SystemInitializer {
	return func(w *ecs.World) ecs.System {
		return &RectDrag{}
	}
}

type RectDrag struct {
	cursorX int
	cursorY int
	drag    bool
	target  ecs.Entity
}

func (s RectDrag) Priority() int64 {
	return 0
}

func (s *RectDrag) Update(world *ecs.World, dt float64) error {
	prevCursorX := s.cursorX
	prevCursorY := s.cursorY
	s.cursorX, s.cursorY = ebiten.CursorPosition()
	click := ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft)

	// クリックしてなければドラッグもしていない
	if !click {
		s.drag = false
		return nil
	}

	cps := ecs.GetComponents[components.DragableRect](world)
	for entity := range cps {
		rect, err := ecs.GetComponent[components.Rect](world, entity)
		if err != nil {
			return err
		}
		if inpututil.IsMouseButtonJustPressed(ebiten.MouseButtonLeft) && rect.OnRect(float64(s.cursorX), float64(s.cursorY)) {
			s.drag = true
			s.target = entity
			break
		}
	}

	if !s.drag {
		return nil
	}

	// ドラッグドロップでの移動
	rect, err := ecs.GetComponent[components.Rect](world, s.target)
	if err != nil {
		return err
	}
	if click {
		rect.X += float64(s.cursorX - prevCursorX)
		rect.Y += float64(s.cursorY - prevCursorY)
	}
	return nil
}
