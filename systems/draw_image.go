package systems

import (
	"graph-adventure/components"
	"graph-adventure/draw"
	"graph-adventure/ecs"

	"github.com/hajimehoshi/ebiten/v2"
)

func GetDefaultDrawImageInitializer(drawer *draw.Drawer) ecs.SystemInitializer {
	return func(w *ecs.World) ecs.System {
		return &DrawImage{
			Drawer: drawer,
		}
	}
}

type DrawImage struct {
	Drawer *draw.Drawer
}

func (s *DrawImage) update(world *ecs.World, entity ecs.Entity, dt float64, component *components.DrawableImageAttachToRect) error {
	rect, err := ecs.GetComponent[components.Rect](world, entity)
	if err != nil {
		return err
	}
	width, height := component.Image.Size()
	op := ebiten.DrawImageOptions{}
	op.GeoM.Scale(rect.Width/float64(width), rect.Height/float64(height))
	op.GeoM.Translate(rect.X, rect.Y)
	component.Drawable.SetImage(component.Image, &op)
	return nil
}

func (s DrawImage) Priority() int64 {
	return 0
}

func (s *DrawImage) Update(world *ecs.World, dt float64) error {
	components := ecs.GetComponents[components.DrawableImageAttachToRect](world)
	for entity, component := range components {
		err := s.update(world, entity, dt, component)
		if err != nil {
			return err
		}
	}
	return nil
}
