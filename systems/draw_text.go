package systems

import (
	"graph-adventure/components"
	"graph-adventure/draw"
	"graph-adventure/ecs"
	"graph-adventure/utils"
	"image/color"
	"strings"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/text"
	"golang.org/x/image/font"
)

func GetDefaultDrawTextOnRectInitializer(drawer *draw.Drawer) ecs.SystemInitializer {
	return func(w *ecs.World) ecs.System {
		return &DrawTextOnRect{
			Drawer: drawer,
		}
	}
}

// On rect
type DrawTextOnRect struct {
	Drawer *draw.Drawer
}

func (s DrawTextOnRect) getTextImageFromFontMetrics(str string, color color.Color, font font.Face) *ebiten.Image {
	textRect := text.BoundString(font, str)
	textImg := ebiten.NewImage(textRect.Min.X+textRect.Max.X, font.Metrics().Height.Floor())
	text.Draw(textImg, str, font, 0, font.Metrics().Ascent.Floor(), color)
	return textImg
}

func (s *DrawTextOnRect) update(world *ecs.World, entity ecs.Entity, dt float64, component *components.DrawableTextOnRect) error {
	rect, err := ecs.GetComponent[components.Rect](world, entity)
	if err != nil {
		return nil
	}
	textComponent, err := ecs.GetComponent[components.Text](world, entity)
	if err != nil {
		return err
	}

	textImage := ebiten.NewImage(int(rect.Width), int(rect.Height))
	texts := strings.Split(textComponent.Text, "\n")
	if len(texts) == 0 {
		texts = []string{""}
	}
	lineHeight := rect.Height / float64(component.Row)

	// カーソル表示
	inputable, err := ecs.GetComponent[components.Inputable](world, entity)
	if err == nil && inputable.ShowCursor && inputable.Active {
		getCursorX := func(row, column int) float64 {
			cursorTextRect := text.BoundString(*component.Font, texts[row][:column])
			return float64(cursorTextRect.Max.X)
		}
		crow, ccolumn := utils.PosToRowColumn(textComponent.Text, inputable.CursorPos)
		cx := getCursorX(crow, ccolumn)
		cy := lineHeight * float64(crow)

		if cx+component.XOffset > component.ViewXOffset+rect.Width {
			slideX := cx
			if ccolumn >= 2 {
				slideX = getCursorX(crow, ccolumn-2)
			}
			component.ViewXOffset = cx*2 - slideX - rect.Width
		} else if cx < component.ViewXOffset {
			component.ViewXOffset = cx
		}
		if cy >= component.ViewYOffset+rect.Height {
			component.ViewYOffset += lineHeight
		} else if cy < component.ViewYOffset {
			component.ViewYOffset -= lineHeight
		}

		// 範囲選択表示
		if inputable.IsRangeSelected() {
			selectStart, selectEnd := inputable.GetSelectRange()
			srow, scolumn := utils.PosToRowColumn(textComponent.Text, selectStart)
			erow, ecolumn := utils.PosToRowColumn(textComponent.Text, selectEnd)

			for i := srow; i <= erow; i++ {
				var sx, ex float64
				if i == srow {
					sx = getCursorX(srow, scolumn)
				}
				if i == erow {
					ex = getCursorX(erow, ecolumn)
				} else {
					ex = getCursorX(i, len(texts[i]))
				}
				ebitenutil.DrawRect(
					textImage,
					sx-component.ViewXOffset+component.CursorStyle.CursorXOffset,
					lineHeight*float64(i)-component.ViewYOffset+component.CursorStyle.CursorYOffset,
					ex-sx,
					component.CursorStyle.CursorHeight,
					component.CursorStyle.RangeSelectColor,
				)
			}
		}

		color := component.TextColor
		if component.CursorStyle.CursorColor != nil {
			color = *component.CursorStyle.CursorColor
		}

		ebitenutil.DrawRect(
			textImage,
			cx-component.ViewXOffset+component.CursorStyle.CursorXOffset,
			cy-component.ViewYOffset+component.CursorStyle.CursorYOffset,
			component.CursorStyle.CursorWidth,
			component.CursorStyle.CursorHeight,
			color,
		)
	}

	// 文字列表示
	for i, txt := range texts {
		if len(txt) == 0 {
			continue
		}
		img := s.getTextImageFromFontMetrics(
			txt,
			component.TextColor,
			*component.Font,
		)
		_, height := img.Size()

		op := ebiten.DrawImageOptions{}
		op.GeoM.Scale(1, lineHeight/float64(height))
		op.GeoM.Translate(
			-component.ViewXOffset,
			lineHeight*float64(i)-component.ViewYOffset,
		)
		textImage.DrawImage(img, &op)
	}

	op := ebiten.DrawImageOptions{}
	op.GeoM.Translate(rect.X, rect.Y)
	component.Drawable.SetImage(textImage, &op)
	return nil
}

func (s DrawTextOnRect) Priority() int64 {
	return 0
}

func (s *DrawTextOnRect) Update(world *ecs.World, dt float64) error {
	components := ecs.GetComponents[components.DrawableTextOnRect](world)
	for entity, component := range components {
		err := s.update(world, entity, dt, component)
		if err != nil {
			return err
		}
	}
	return nil
}

// Attach to rect
func GetDefaultDrawTextAttachToRectInitializer(drawer *draw.Drawer) ecs.SystemInitializer {
	return func(w *ecs.World) ecs.System {
		return &DrawTextAttachToRect{
			Drawer: drawer,
		}
	}
}

type DrawTextAttachToRect struct {
	Drawer *draw.Drawer
}

func (s DrawTextAttachToRect) getTextImageWithSize(str string, color color.Color, font font.Face, width, height, ascent int) *ebiten.Image {
	textImg := ebiten.NewImage(width, height)
	text.Draw(textImg, str, font, 0, ascent, color)
	return textImg
}

func (s *DrawTextAttachToRect) getTextImageFromStringBound(str string, color color.Color, font font.Face) *ebiten.Image {
	textRect := text.BoundString(font, str)
	return s.getTextImageWithSize(str, color, font, textRect.Min.X+textRect.Max.X, -textRect.Min.Y+textRect.Max.Y, -textRect.Min.Y)
}

func (s *DrawTextAttachToRect) update(world *ecs.World, entity ecs.Entity, dt float64, component *components.DrawableTextAttachToRect) error {
	txt, err := ecs.GetComponent[components.Text](world, entity)
	if err != nil {
		return err
	}
	rect, err := ecs.GetComponent[components.Rect](world, entity)
	if err != nil {
		return err
	}
	textImage := s.getTextImageFromStringBound(
		txt.Text,
		component.TextColor,
		*component.Font,
	)
	width, height := textImage.Size()

	op := ebiten.DrawImageOptions{}
	op.GeoM.Scale(rect.Width/float64(width), rect.Height/float64(height))
	op.GeoM.Translate(-rect.Width/2, -rect.Height/2)
	op.GeoM.Scale(component.ScaleX, component.ScaleY)
	op.GeoM.Translate(rect.X+rect.Width/2, rect.Y+rect.Height/2)
	component.Drawable.SetImage(textImage, &op)
	return nil
}

func (s DrawTextAttachToRect) Priority() int64 {
	return 0
}

func (s *DrawTextAttachToRect) Update(world *ecs.World, dt float64) error {
	components := ecs.GetComponents[components.DrawableTextAttachToRect](world)
	for entity, component := range components {
		err := s.update(world, entity, dt, component)
		if err != nil {
			return err
		}
	}
	return nil
}
