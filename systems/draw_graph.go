package systems

import (
	"graph-adventure/components"
	"graph-adventure/draw"
	"graph-adventure/ecs"
	"graph-adventure/formula"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

func GetDefaultDrawGraphInitializer(drawer *draw.Drawer) ecs.SystemInitializer {
	return func(w *ecs.World) ecs.System {
		return &DrawGraph{
			Drawer: drawer,
		}
	}
}

type DrawGraph struct {
	Drawer *draw.Drawer
}

func (s *DrawGraph) update(world *ecs.World, entity ecs.Entity, dt float64, component *components.DrawableGraphAttachToRect) error {
	rect, err := ecs.GetComponent[components.Rect](world, entity)
	if err != nil {
		return err
	}
	graphImage := ebiten.NewImage(int(rect.Width), int(rect.Height))

	rateX := rect.Width / component.SizeX
	rateY := rect.Height / component.SizeY

	step := 1.0
	if component.Resolution > 0 {
		step = component.SizeX / float64(component.Resolution)
	}

	for _, graph := range component.Graphs {
		var value float64
		calc := func(x float64) float64 {
			component.Variables[graph.TransitionVariable] = x - component.SizeX/2 + graph.OriginX
			return formula.Calculation(graph.Expression, component.Variables) - component.SizeY/2 + graph.OriginY
		}
		for i := -step; i < component.SizeX+step; i += step {
			prevValue := value
			value = calc(i)
			if err != nil {
				break
			}
			prevViewY := prevValue * -rateY
			viewY := value * -rateY
			if (0 < prevViewY && prevViewY < rect.Height) ||
				(0 < viewY && viewY < rect.Height) {
				prevViewX := (i - step) * rateX
				viewX := i * rateX
				ebitenutil.DrawLine(
					graphImage,
					prevViewX,
					prevViewY,
					viewX,
					viewY,
					graph.GraphColor,
				)
			}
		}
	}
	op := ebiten.DrawImageOptions{}
	op.GeoM.Translate(rect.X, rect.Y)
	component.Drawable.SetImage(graphImage, &op)
	return nil
}

func (s DrawGraph) Priority() int64 {
	return 0
}

func (s *DrawGraph) Update(world *ecs.World, dt float64) error {
	components := ecs.GetComponents[components.DrawableGraphAttachToRect](world)
	for entity, component := range components {
		err := s.update(world, entity, dt, component)
		if err != nil {
			return err
		}
	}
	return nil
}
