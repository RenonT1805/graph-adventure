package systems

import (
	"graph-adventure/components"
	"graph-adventure/draw"
	"graph-adventure/ecs"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

func GetDefaultDrawRectInitializer(drawer *draw.Drawer) ecs.SystemInitializer {
	return func(w *ecs.World) ecs.System {
		return &DrawRect{
			Drawer: drawer,
		}
	}
}

type DrawRect struct {
	Drawer *draw.Drawer
}

func (s *DrawRect) update(world *ecs.World, entity ecs.Entity, dt float64, component *components.DrawableRect) error {
	rect, err := ecs.GetComponent[components.Rect](world, entity)
	if err != nil {
		return err
	}
	img := ebiten.NewImage(int(rect.Width), int(rect.Height))
	borderWidth := component.BorderWidth
	if borderWidth > 0 {
		ebitenutil.DrawRect(
			img,
			0,
			0,
			rect.Width,
			rect.Height,
			component.BorderColor,
		)
	}
	ebitenutil.DrawRect(
		img,
		borderWidth,
		borderWidth,
		rect.Width-borderWidth*2,
		rect.Height-borderWidth*2,
		component.BackgroundColor,
	)
	op := ebiten.DrawImageOptions{}
	op.GeoM.Translate(rect.X, rect.Y)
	component.Drawable.SetImage(img, &op)
	return nil
}

func (s DrawRect) Priority() int64 {
	return 0
}

func (s *DrawRect) Update(world *ecs.World, dt float64) error {
	components := ecs.GetComponents[components.DrawableRect](world)
	for entity, component := range components {
		err := s.update(world, entity, dt, component)
		if err != nil {
			return err
		}
	}
	return nil
}
