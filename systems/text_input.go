package systems

import (
	"graph-adventure/components"
	"graph-adventure/ecs"
	"graph-adventure/utils"
	"strings"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

func GetDefaultTextInputInitializer() ecs.SystemInitializer {
	return func(w *ecs.World) ecs.System {
		return &TextInput{
			targets:         map[ecs.Entity]*components.Inputable{},
			pressingKeyTime: map[ebiten.Key]int{},
		}
	}
}

type TextInput struct {
	targets         map[ecs.Entity]*components.Inputable
	pressingKeyTime map[ebiten.Key]int
}

func (s TextInput) Priority() int64 {
	return 0
}

func (s *TextInput) Update(world *ecs.World, dt float64) error {
	cursorX, cursorY := ebiten.CursorPosition()
	justClick := inpututil.IsMouseButtonJustPressed(ebiten.MouseButtonLeft)

	cps := ecs.GetComponents[components.Inputable](world)
	for entity, component := range cps {
		rect, err := ecs.GetComponent[components.Rect](world, entity)
		if err != nil {
			return err
		}
		if component.Always {
			s.targets[entity] = component
			component.Active = true
		} else if justClick && rect.OnRect(float64(cursorX), float64(cursorY)) {
			s.targets[entity] = component
			component.Active = true
		} else if justClick {
			s.targets[entity] = nil
			component.Active = false
		}
	}

	if len(s.targets) == 0 {
		return nil
	}

	// ボタン押し続けの処理
	inputKeys := []ebiten.Key{}
	inputKeys = inpututil.AppendPressedKeys(inputKeys)
	pressKeys := map[ebiten.Key]int{}
	for _, inputKey := range inputKeys {
		if time, exist := s.pressingKeyTime[inputKey]; exist {
			pressKeys[inputKey] = time
		}
		pressKeys[inputKey] += int(dt * 1000.0)
	}
	s.pressingKeyTime = pressKeys

	// キーボードからの入力
	for target, component := range s.targets {
		if component == nil {
			continue
		}

		// テキストを改行で分割
		textComponent, err := ecs.GetComponent[components.Text](world, target)
		if err != nil {
			return err
		}

		// 範囲選択
		shift := ebiten.IsKeyPressed(ebiten.KeyShift)
		if !component.IsRangeSelected() {
			if shift {
				component.SetMark(component.CursorPos)
			} else {
				component.ResetMark()
			}
		}

		// 通常入力
		input := func(str string) {
			selectStart := component.CursorPos
			selectEnd := component.CursorPos
			if component.IsRangeSelected() {
				selectStart, selectEnd = component.GetSelectRange()
			}
			textComponent.Text = utils.RemoveString(textComponent.Text, selectStart, selectEnd)
			textComponent.Text = utils.InsertString(textComponent.Text, str, selectStart)
			component.CursorPos = selectStart + len(str)
			component.ResetMark()
		}
		inputChars := []rune{}
		inputChars = ebiten.AppendInputChars(inputChars)
		for _, char := range inputChars {
			input(string(char))
		}

		isPressing := func(key ebiten.Key) bool {
			return inpututil.IsKeyJustPressed(key) ||
				(s.pressingKeyTime[key] > component.PressingTime && s.pressingKeyTime[key]%component.PressingInputInterval == 0)
		}

		// 改行
		if component.AllowNewLine && isPressing(ebiten.KeyEnter) {
			input("\n")
		}

		// バックスペース
		if isPressing(ebiten.KeyBackspace) {
			if !component.IsRangeSelected() && component.CursorPos > 0 {
				component.ResetMark()
				component.SetMark(component.CursorPos - 1)
			}
			input("")
		}

		// デリート
		if isPressing(ebiten.KeyDelete) {
			if !component.IsRangeSelected() && component.CursorPos < len(textComponent.Text) {
				component.ResetMark()
				component.SetMark(component.CursorPos + 1)
			}
			input("")
		}

		// 左右キー
		if isPressing(ebiten.KeyLeft) {
			if !shift && component.IsRangeSelected() {
				component.CursorPos, _ = component.GetSelectRange()
				component.ResetMark()
			} else if component.CursorPos > 0 {
				component.CursorPos--
			}
		}
		if isPressing(ebiten.KeyRight) {
			if !shift && component.IsRangeSelected() {
				_, component.CursorPos = component.GetSelectRange()
				component.ResetMark()
			} else if component.CursorPos < len(textComponent.Text) {
				component.CursorPos++
			}
		}

		// 上下キー
		texts := strings.Split(textComponent.Text, "\n")
		row, column := utils.PosToRowColumn(textComponent.Text, component.CursorPos)
		if isPressing(ebiten.KeyUp) && row > 0 {
			if !shift && component.IsRangeSelected() {
				component.ResetMark()
			}
			row--
			if column > len(texts[row]) {
				column = len(texts[row])
			}
			if !shift && component.IsRangeSelected() {
				component.ResetMark()
			}
		}
		if isPressing(ebiten.KeyDown) && row+1 < len(texts) {
			row++
			if column > len(texts[row]) {
				column = len(texts[row])
			}
			if !shift && component.IsRangeSelected() {
				component.ResetMark()
			}
		}
		component.CursorPos = utils.RowColumnToPos(textComponent.Text, row, column)
	}
	return nil
}
