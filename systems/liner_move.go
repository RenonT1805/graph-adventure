package systems

import (
	"graph-adventure/components"
	"graph-adventure/ecs"
)

func GetDefaultLinerMoveInitializer() ecs.SystemInitializer {
	return func(w *ecs.World) ecs.System {
		return LinerMove{}
	}
}

type LinerMove struct {
}

func (l LinerMove) update(world *ecs.World, dt float64, component *components.MoveVector) error {
	pos, err := ecs.GetComponent[components.Position](world, component.Entity)
	if err != nil {
		return err
	}
	pos.X += component.X
	pos.Y += component.Y
	return nil
}

func (l LinerMove) Priority() int64 {
	return 0
}

func (l LinerMove) Update(world *ecs.World, dt float64) error {
	components := ecs.GetComponents[components.MoveVector](world)
	for _, component := range components {
		err := l.update(world, dt, component)
		if err != nil {
			return err
		}
	}
	return nil
}
